package provide.api.project.spring.boot.business;

import org.springframework.stereotype.Service;
import provide.api.project.spring.boot.entity.Customer;
import provide.api.project.spring.boot.interfaces.IBusiness;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService implements IBusiness {

    @Override
    public Customer getCustomerByName(String name) {
        List<Customer> listCust = listAllCustomer();
        List<Customer> collect = listCust
                .stream()
                .filter(cust -> cust.getName() != null && cust.getName().equals(name))
                .collect(Collectors.toList());
        return collect.size() > 0 ? collect.get(0) : null;
    }

    private List<Customer> listAllCustomer() {
        List<Customer> listCust = new ArrayList<>();
        Customer cus = new Customer();
        cus.setName("VIET");
        cus.setAge(32);

        Customer cus1 = new Customer();
        cus1.setName("NINH");
        cus1.setAge(31);

        listCust.add(cus);
        listCust.add(cus1);

        return listCust;
    }
}
