package provide.api.project.spring.boot.interfaces;

import provide.api.project.spring.boot.entity.Customer;

public interface IBusiness {
    Customer getCustomerByName(String name);

}
