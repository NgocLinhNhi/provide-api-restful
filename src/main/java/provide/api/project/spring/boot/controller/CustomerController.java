package provide.api.project.spring.boot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import provide.api.project.spring.boot.business.CustomerService;
import provide.api.project.spring.boot.entity.Customer;

@Controller
@RequestMapping("api/customer")
public class CustomerController {
    private Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Value("${server.port}")
    String port;

    @Value("${password}")
    String password;

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "/get", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody //phải có Response Body không thì bên callAPi ăn cýt
    public Customer getCustomer() {
        Customer cus = new Customer("Viet", 34);
        logger.info(" Response for Client === Customer info {}", cus);
        return cus;
    }

    @GetMapping(value = "/getv1", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Customer getCustomerV1() {
        Customer cus = new Customer("NINH", 31);
        logger.info(" Response for Client ===  Customer info NAME : {}  AGE: {}", cus.getName(), cus.getAge());
        return cus;
    }

    //Get rest api with PathVariable
    @GetMapping(value = "/get/{name}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<Customer> getCustomerById(@PathVariable("name") String name) {
        logger.info(" =============== GET METHOD PathVariable CALL API");

        Customer cust = customerService.getCustomerByName(name);
        if (cust != null) {
            return new ResponseEntity<>(cust, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //Post method rest api with RequestBody (PostMapping)
    @PostMapping(value = "/post/requestbody")
    @ResponseBody
    public Customer postCustomerByIdRequestBody(@RequestBody Customer customer) {
        logger.info(" ============== POST METHOD CALL API BY REQUEST BODY");
        Customer cust = customerService.getCustomerByName(customer.getName());

        if (cust != null) {
            logger.info(" Response for Client ===  CUSTOMER INFO {} ", cust.getName());
        } else {
            logger.info(" Response for Client ===  CUSTOMER IS NOT FOUND ");
        }
        return cust;
    }

    //GET method rest api with request parameter(GetMapping) / RequestBody (PostMapping)
    //request param không cần set  {name} ở value GetMapping như PathVariable -> nhưng cần set value = "name"  ở Request Param nó mới hiểu
    @GetMapping(value = "/getv1/requestparam")
    @ResponseBody
    public Customer getCustomerByIdRequestParam(@RequestParam(value = "name", required = false) String name) {
        logger.info("========== GET METHOD REQUEST PARAM CALL API");
        logger.info("=========== request param :" + name);

        Customer cust = customerService.getCustomerByName(name);
        if (cust != null) {
            logger.info(" Response for Client ===  CUSTOMER INFO {} ", cust.getName());
        } else {
            logger.info(" Response for Client === CUSTOMER IS NOT FOUND ");
        }
        return cust;
    }


}
