package provide.api.project.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvideApiProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvideApiProjectApplication.class, args);
    }

}
